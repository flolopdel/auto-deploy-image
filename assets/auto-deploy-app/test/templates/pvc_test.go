package main

import (
	"regexp"
	"testing"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/stretchr/testify/require"
	appsV1 "k8s.io/api/apps/v1"
	coreV1 "k8s.io/api/core/v1"
)

func TestPVC_DisabledByDefault(t *testing.T) {

	templates := []string{"templates/pvc.yaml"}

	opts := map[string]string{}

	renderTemplate(t, opts, "test", templates, regexp.MustCompile("Error: could not find template templates/pvc.yaml in chart"))

}

func TestPVC_UsingExistingClaim(t *testing.T) {

	templates := []string{"templates/pvc.yaml"}

	opts := map[string]string{
		"persistence.existingClaim": "testing",
	}

	renderTemplate(t, opts, "test", templates, regexp.MustCompile("Error: could not find template templates/pvc.yaml in chart"))

}

func TestPVC_EnabledWithDefaults(t *testing.T) {

	var pvc coreV1.PersistentVolumeClaim

	renderTemplateType(t, "templates/pvc.yaml", map[string]string{
		"persistence.enabled": "true",
	}, &pvc)

	require.Equal(t, coreV1.PersistentVolumeAccessMode("ReadWriteOnce"), pvc.Spec.AccessModes[0])
	storage := pvc.Spec.Resources.Requests[coreV1.ResourceStorage]
	require.Equal(t, "8Gi", (&storage).String())
	//require.Equal(t, "", *pvc.Spec.StorageClass)
}

func TestPVC_OverrideAccessMode(t *testing.T) {

	var pvc coreV1.PersistentVolumeClaim

	renderTemplateType(t, "templates/pvc.yaml", map[string]string{
		"persistence.enabled": "true",
		"persistence.accessMode": "ReadOnlyMany",
	}, &pvc)

	require.Equal(t, coreV1.PersistentVolumeAccessMode("ReadOnlyMany"), pvc.Spec.AccessModes[0])
}

func TestPVC_OverrideStorageClass(t *testing.T) {

	var pvc coreV1.PersistentVolumeClaim

	renderTemplateType(t, "templates/pvc.yaml", map[string]string{
		"persistence.enabled": "true",
		"persistence.storageClass": "testing",
	}, &pvc)

	//require.Equal(t, "testing", *pvc.Spec.StorageClass)
}

func TestPVC_OverrideStorageSize(t *testing.T) {

	var pvc coreV1.PersistentVolumeClaim

	renderTemplateType(t, "templates/pvc.yaml", map[string]string{
		"persistence.enabled": "true",
		"persistence.size": "1Gi",
	}, &pvc)

	storage := pvc.Spec.Resources.Requests[coreV1.ResourceStorage]
	require.Equal(t, "1Gi", (&storage).String())
}

func TestPVCVolume_DisabledByDefault(t *testing.T) {

	var deployment appsV1.Deployment

	renderTemplateType(t, "templates/deployment.yaml", map[string]string{}, &deployment)

	require.Len(t, deployment.Spec.Template.Spec.Volumes, 0)
	require.Len(t, deployment.Spec.Template.Spec.Containers[0].VolumeMounts, 0)
}

func TestPVCVolume_EnabledWithDefaults(t *testing.T) {

	var deployment appsV1.Deployment

	renderTemplateType(t, "templates/deployment.yaml",
		map[string]string{
		"persistence.enabled": "true",
	}, &deployment)

	require.Len(t, deployment.Spec.Template.Spec.Volumes, 1)
	require.Len(t, deployment.Spec.Template.Spec.Containers[0].VolumeMounts, 1)
	require.Equal(t, "test-auto-deploy", deployment.Spec.Template.Spec.Volumes[0].PersistentVolumeClaim.ClaimName)
	require.Equal(t, "/pvc-mount", deployment.Spec.Template.Spec.Containers[0].VolumeMounts[0].MountPath)
}

func TestPVCVolume_OverrideClaimName(t *testing.T) {

	var deployment appsV1.Deployment

	renderTemplateType(t, "templates/deployment.yaml", map[string]string{
		"persistence.enabled": "true",
		"persistence.existingClaim": "testing",
	}, &deployment)

	require.Equal(t, "testing", deployment.Spec.Template.Spec.Volumes[0].PersistentVolumeClaim.ClaimName)
}

func TestPVCVolume_OverrideMountPath(t *testing.T) {

	var deployment appsV1.Deployment

	renderTemplateType(t, "templates/deployment.yaml", map[string]string{
		"persistence.enabled": "true",
		"persistence.mountPath": "/testing",
	}, &deployment)

	require.Equal(t, "/testing", deployment.Spec.Template.Spec.Containers[0].VolumeMounts[0].MountPath)
}

func TestWorkerPVCVolume_DisabledByDefault(t *testing.T) {

	var deployment appsV1.DeploymentList

	renderTemplateType(t, "templates/worker-deployment.yaml", map[string]string{
		"workers.worker1.command[0]": "echo",
		"workers.worker1.command[1]": "worker1",
	}, &deployment)

	require.Len(t, deployment.Items[0].Spec.Template.Spec.Volumes, 0)
	require.Len(t, deployment.Items[0].Spec.Template.Spec.Containers[0].VolumeMounts, 0)
}

func TestWorkerPVCVolume_EnabledWithDefaults(t *testing.T) {

	var deployment appsV1.DeploymentList

	renderTemplateType(t,"templates/worker-deployment.yaml", map[string]string{
		"workers.worker1.command[0]": "echo",
		"workers.worker1.command[1]": "worker1",
		"persistence.enabled": "true",
	}, &deployment)

	require.Len(t, deployment.Items[0].Spec.Template.Spec.Volumes, 1)
	require.Len(t, deployment.Items[0].Spec.Template.Spec.Containers[0].VolumeMounts, 1)
	require.Equal(t, "test-auto-deploy", deployment.Items[0].Spec.Template.Spec.Volumes[0].PersistentVolumeClaim.ClaimName)
	require.Equal(t, "/pvc-mount", deployment.Items[0].Spec.Template.Spec.Containers[0].VolumeMounts[0].MountPath)
}

func TestWorkerPVCVolume_OverrideClaimName(t *testing.T) {

	var deployment appsV1.DeploymentList

	renderTemplateType(t, "templates/worker-deployment.yaml", map[string]string{
		"workers.worker1.command[0]": "echo",
		"workers.worker1.command[1]": "worker1",
		"persistence.enabled": "true",
		"persistence.existingClaim": "testing",
	}, &deployment)

	require.Equal(t, "testing", deployment.Items[0].Spec.Template.Spec.Volumes[0].PersistentVolumeClaim.ClaimName)
}

func TestWorkerPVCVolume_OverrideMountPath(t *testing.T) {

	var deployment appsV1.DeploymentList

	renderTemplateType(t, "templates/worker-deployment.yaml", map[string]string{
		"workers.worker1.command[0]": "echo",
		"workers.worker1.command[1]": "worker1",
		"persistence.enabled":        "true",
		"persistence.mountPath":      "/testing",
	}, &deployment)

	require.Equal(t, "/testing", deployment.Items[0].Spec.Template.Spec.Containers[0].VolumeMounts[0].MountPath)

}

func renderTemplateType(t *testing.T, template string, values map[string]string, destination interface{}) {

	templates := []string{template}

	opts := &helm.Options{
		SetValues: values,
	}

	output := helm.RenderTemplate(t, opts, helmChartPath, "test", templates)

	helm.UnmarshalK8SYaml(t, output, &destination)

}